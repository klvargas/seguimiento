﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Prueba1
{
    public partial class Indicador_valor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int intervalo = (DateTime.Now.Year) - 2018;
            int ano_inicial = 2018;
            cmb_ano.Items.Clear();
            cmb_ano.Items.Insert(0,"2018");
            cmb_ano.Items[0].Value = "2018";


            for (int i = 1; i <= intervalo; i++)
            {
                ano_inicial = ano_inicial + 1;
                cmb_ano.Items.Insert(i,(ano_inicial).ToString());                
                cmb_ano.Items[i].Value= ano_inicial.ToString();
            }
        }

        protected void cmb_indicador_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            
            //int ano1 = Convert.ToInt32(txtano.Text);

            //if (txtano.Text.Length==0)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Debe digitar el año a evaluar')", true);

            //}

            seguimientoEntities cnx2 = new seguimientoEntities();
            seguimientoEntities cnx1 = new seguimientoEntities();
            seguimientoEntities cnx3 = new seguimientoEntities();
            seguimientoEntities cnx4 = new seguimientoEntities();
            seguimientoEntities cnx5 = new seguimientoEntities();
            indicador_periodos ind_period = new indicador_periodos();
            periodos periodo = new periodos();
            int id_ind = Convert.ToInt32(cmb_indicador.SelectedValue);
            int id_periodicidad;
            
            Boolean crear_preiodos = false;
            using (cnx1)
            {
                double ind = Convert.ToDouble(cmb_indicador.SelectedValue);

                var query = (from w in cnx1.indicadores
                             where w.id_ind == ind
                             select w).FirstOrDefault();

                id_periodicidad = query.id_period.Value;
                Edit_Valor.NumericTextBox.MinValue= Convert.ToDouble(query.valor_min);
                Edit_Valor.NumericTextBox.MaxValue = Convert.ToDouble(query.valor_max);

            }


            using (cnx2)
            {
                var periodos_temp = (from w in cnx2.indicador_periodos
                                     where w.id_indic == id_ind
                                     select w).FirstOrDefault();
                if (periodos_temp == null)
                {
                    crear_preiodos = true;
                }
            }

            if (crear_preiodos == true)
            {

                using (cnx4)
                using (cnx3)
                {
                    var perio = (from w in cnx3.periodos
                                 where w.id_periodic == id_periodicidad
                                 select new { w.id, w.nombre });

                    foreach (var group in perio)
                    {
                        ind_period.id_indic = id_ind;
                        ind_period.id_periodicidad = id_periodicidad;
                        ind_period.id_periodo = group.id;
                        ind_period.id_valor = 0;
                        ind_period.ano = Convert.ToInt32(cmb_ano.SelectedValue);
                        cnx4.indicador_periodos.Add(ind_period);
                        cnx4.SaveChanges();
                    }

                }

                //RadGrid1.MasterTableView.GetColumn("combo_columna").da

            }

            RadGrid1.DataSourceID = null;
            RadGrid1.DataSourceID = "EDS_PERIODOS";

        //    using (cnx5)
        //    {
        //        var seg = (from w in cnx5.indicador_valor
        //                   where w.id_ind == id_ind && w.ano == ano1
        //                   orderby w.id_periodo ascending
        //                   select new { w.id_periodo });
        //        int ind = 1;
        //        int peri;
        //        bool error = false;
        //        foreach (var group in seg)
        //        {
        //            if (ind == 1)
        //            {
        //                peri = 
        //                peri = peri + 1;
        //                ind = ind * 1;
        //            }
        //            else
        //            {
        //                if (group. != peri)
        //                {
        //                    error = true;
        //                }
        //            }
        //        }
        //        if (error == true)
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltas periodos intermedios por valorar')", true);

        //        }

        //    }
        }
    
        protected void btn_evaluar_Click(object sender, EventArgs e)
        {
            seguimientoEntities cnx1 = new seguimientoEntities();
            seguimientoEntities cnx2 = new seguimientoEntities();
            indicadores indicador = new indicadores();
            rangos_eval rango = new rangos_eval();
            int id_ind = Convert.ToInt32(cmb_indicador.SelectedValue);
            int ano = Convert.ToInt32(cmb_ano.SelectedValue);

            using (cnx1)
            {
                var seg = (from w in cnx1.indicador_periodos
                           where w.id_indic== id_ind && w.ano == ano
                           orderby w.id_periodo ascending
                           select new { w.id_periodo, w.id_valor, w.indicadores.valor_max});
                int ind = 0;
                int peri = 0;
                bool error = false;
                bool valor_nulo = false;
                double acum = 0;
                double porcen_acumulado = 0;
                double evalua;
                double promedio;

                foreach (var group in seg)
                {
                    acum = acum + group.id_valor.Value;
                    porcen_acumulado = porcen_acumulado + ((Convert.ToDouble( group.id_valor.Value) / Convert.ToDouble( group.valor_max.Value)) * 100);

                    if (valor_nulo == false)
                    {
                        if(group.id_valor == 0)
                        {
                            valor_nulo = true;
                        }
                        else
                        {
                            ind = ind + 1;
                        }
                    }
                    else
                    {
                        if(group.id_valor > 0)
                        {
                            error = true;
                        }
                    }
                }
                if (error == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltas periodos intermedios por valorar')", true);
                    txtevalua.Text = "";
                    txt_prom.Value = null;
                }
                else
                {

                    evalua = porcen_acumulado / ind;
                    promedio = acum / ind;
                    txt_prom.Value = promedio;
                    //txtevalua.Text = Convert.ToString(evalua);
                    using (cnx2)
                    {
                        var query = (from w in cnx2.rangos_eval
                                     where w.valor_min <= evalua && w.valor_max >= evalua
                                     select w).FirstOrDefault();

                        txtevalua.Text = Convert.ToString(query.calificacion);
                    }
                    Panel1.Visible = true;

                }

            }
        }
        protected void RadComboBox1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void cmb_acciones_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }

        //protected void btn_evaluar_Click(object sender, EventArgs e)
        //{


        //}
    }
}
