﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Agregar_usuario.aspx.cs" Inherits="Prueba1.Agregar_usuario" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Usuario</h3>
              </div>
             
            </
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">    
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Información del Usuario</span>
                           <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre del Usuario </label> <br />
                       
                        <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtnombre" runat="server" style="left: 0px; top: 0px" Width="66%"></asp:TextBox>
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Dirección </label><br />
                        
                        <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtdireccion" runat="server" style="left: 0px; top: 0px" Width="66%"></asp:TextBox>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cargo</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione un Cargo" ID="cmb_cargo" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_cargo" DataTextField="nombre" DataValueField="id_cargo" Sort="Ascending">
                      </telerik:RadComboBox>
                           <asp:EntityDataSource ID="EDS_cargo" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableFlattening="False" EntitySetName="cargo">
                           </asp:EntityDataSource>
                    </div>
                   </div>
                           </telerik:RadAjaxPanel>
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btn_guardar_Click" />
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</asp:Content>
