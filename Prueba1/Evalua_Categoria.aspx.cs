﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Prueba1
{
    public partial class Evalua_Categoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            int intervalo = (DateTime.Now.Year) - 2018;
            int ano_inicial = 2018;
            cmb_ano.Items.Clear();
            cmb_ano.Items.Insert(0, "2018");
            cmb_ano.Items[0].Value = "2018";


            for (int i = 1; i <= intervalo; i++)
            {
                ano_inicial = ano_inicial + 1;
                cmb_ano.Items.Insert(i, (ano_inicial).ToString());
                cmb_ano.Items[i].Value = ano_inicial.ToString();
            }
        }

        protected void btn_evaluar_Click(object sender, EventArgs e)
        {
            int id_cat = Convert.ToInt32(cmb_categoria.SelectedValue);
            int ano = Convert.ToInt32(cmb_ano.SelectedValue);
            seguimientoEntities cnx = new seguimientoEntities();
            seguimientoEntities cnx1 = new seguimientoEntities();
            seguimientoEntities cnx2 = new seguimientoEntities();
            seguimientoEntities cnx3 = new seguimientoEntities();
            evalua_categoria eval = new evalua_categoria();
            RadGrid1.DataSourceID = null;

            using (cnx)
            {
                var evalua_tmp = (from w in cnx.evalua_categoria
                                  where w.id_cat == id_cat
                                  select w);
                if (evalua_tmp != null)
                {
                    foreach (var group in evalua_tmp)
                    {
                        cnx.evalua_categoria.Remove(group);

                    }
                }
                cnx.SaveChanges();
            }
            using (cnx1)
            {
                var q = (from w in cnx1.acciones
                         where w.id_cat == id_cat
                         select new { w.id_accion, w.nombre_accion });
                using (cnx2)
                {
                    
                    bool error = false;
                    double evalua;
                    double promedio;
                    double evalua_total = 0;
                    double acum_accion = 0;
                    double num_accion = 0;
                    double evalua_accion = 0;

                    foreach (var group in q)
                    {
                        int num_indic = 0;
                        double acum = 0;
                        double porcen_acumulado = 0;
                        int ind = 0;
                        double promedio_acum = 0;
                        double evalua_acum = 0;


                        var seg = (from w in cnx2.indicadores
                                   where w.id_acc == @group.id_accion
                                   select new { w.id_ind, w.nombre_ind });

                        foreach (var group1 in seg)
                        {
                            bool valor_nulo = false;

                            var seg1 = (from w in cnx3.indicador_periodos
                                        where w.id_indic == @group1.id_ind && w.ano == ano
                                        select new { w.id_periodo, w.id_valor, w.indicadores.valor_max });

                            foreach (var group2 in seg1)
                            {
                                acum = acum + group2.id_valor.Value;
                                porcen_acumulado = porcen_acumulado + ((Convert.ToDouble(group2.id_valor.Value) / Convert.ToDouble(group2.valor_max.Value)) * 100);

                                if (valor_nulo == false)
                                {
                                    if (group2.id_valor == 0)
                                    {
                                        valor_nulo = true;
                                    }
                                    else
                                    {
                                        ind = ind + 1;
                                    }
                                }
                                else
                                {
                                    if (group2.id_valor > 0)
                                    {
                                        error = true;
                                    }
                                }
                            }
                            if (error == true)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltas periodos intermedios por valorar')", true);

                            }
                            else
                            {
                                evalua = porcen_acumulado / ind;
                                promedio = acum / ind;
                                evalua_acum = evalua_acum + evalua;
                                promedio_acum = promedio_acum + promedio;
                            }
                            num_indic = num_indic + 1;
                        }
                        num_accion = num_accion + 1;

                        if (num_indic == 0)
                        {
                            evalua_accion = 0;
                        }
                        else
                        {

                            evalua_accion = evalua_acum / num_indic;
                        }
                            acum_accion = acum_accion + evalua_accion;
                            eval.id_cat = id_cat;
                            eval.nom_acc = group.nombre_accion;
                            eval.ano = ano;
                            eval.id_acc = group.id_accion;
                            eval.evaluacion = evalua_accion;
                            cnx2.evalua_categoria.Add(eval);
                            cnx2.SaveChanges();
                        
                    }

                    evalua_total = acum_accion / num_accion;
                    txtevalua.Value = evalua_total;
                }

                RadGrid1.DataSourceID = null;
                RadGrid1.DataSourceID = "EDS_evalua";
                Panel1.Visible = true;
            }
        }
    }
}