//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Prueba1
{
    using System;
    using System.Collections.Generic;
    
    public partial class cargo
    {
        public cargo()
        {
            this.funcionario = new HashSet<funcionario>();
        }
    
        public int id_cargo { get; set; }
        public string nombre { get; set; }
    
        public virtual ICollection<funcionario> funcionario { get; set; }
    }
}
