﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Prueba1
{
    public partial class Evalua_Accion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            int intervalo = (DateTime.Now.Year) - 2018;
            int ano_inicial = 2018;
            cmb_ano.Items.Clear();
            cmb_ano.Items.Insert(0, "2018");
            cmb_ano.Items[0].Value = "2018";
            
            for (int i = 1; i <= intervalo; i++)
            {
                ano_inicial = ano_inicial + 1;
                cmb_ano.Items.Insert(i, (ano_inicial).ToString());
                cmb_ano.Items[i].Value = ano_inicial.ToString();
            }
        }

        protected void btn_evaluar_Click(object sender, EventArgs e)
        {
            int id_accion = Convert.ToInt32(cmb_accion.SelectedValue);
            int ano = Convert.ToInt32(cmb_ano.SelectedValue);
            seguimientoEntities cnx = new seguimientoEntities();
            seguimientoEntities cnx1 = new seguimientoEntities();
            seguimientoEntities cnx2 = new seguimientoEntities();
            seguimientoEntities cnx3 = new seguimientoEntities();
            seguimientoEntities cnx4 = new seguimientoEntities();
            seguimientoEntities cnx5 = new seguimientoEntities();
            evalua_accion eval = new evalua_accion();
            RadGrid1.DataSourceID = null;
            using (cnx5)
            {
                var evalua_tmp = (from w in cnx5.evalua_accion
                                  where w.id_accion == id_accion
                                  select w);
                if (evalua_tmp != null)
                {
                    foreach (var group in evalua_tmp)
                    {
                        cnx5.evalua_accion.Remove(group);
                        
                    }
                }
                cnx5.SaveChanges();
            }
                using (cnx)
                {
                    var q = (from w in cnx.indicadores
                             where w.id_acc == id_accion
                             select new { w.id_ind, w.nombre_ind });

                    using (cnx2)
                    {
                        int ind = 0;
                        bool error = false;
                        double acum = 0;
                        double porcen_acumulado = 0;
                        double evalua;
                        double promedio;
                        double evalua_acum = 0;
                        double promedio_acum = 0;
                        double evalua_total = 0;
                        int num_indic = 0;

                        foreach (var group in q)
                        {

                            bool valor_nulo = false;
                            var seg = (from w in cnx2.indicador_periodos
                                       where w.id_indic == @group.id_ind && w.ano == ano
                                       select new { w.id_periodo, w.id_valor, w.indicadores.valor_max });

                            foreach (var group1 in seg)
                            {
                                acum = acum + group1.id_valor.Value;
                                porcen_acumulado = porcen_acumulado + ((Convert.ToDouble(group1.id_valor.Value) / Convert.ToDouble(group1.valor_max.Value)) * 100);

                                if (valor_nulo == false)
                                {
                                    if (group1.id_valor == 0)
                                    {
                                        valor_nulo = true;
                                    }
                                    else
                                    {
                                        ind = ind + 1;
                                    }
                                }
                                else
                                {
                                    if (group1.id_valor > 0)
                                    {
                                        error = true;
                                    }
                                }
                            }
                            if (error == true)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltas periodos intermedios por valorar')", true);

                            }
                            else
                            {
                                evalua = porcen_acumulado / ind;
                                promedio = acum / ind;
                                evalua_acum = evalua_acum + evalua;
                                promedio_acum = promedio_acum + promedio;

                                eval.id_indicador = group.id_ind;
                                eval.nom_indicador = group.nombre_ind;
                                eval.evaluacion = evalua;
                                eval.promedio = promedio;
                                eval.ano = ano;
                                eval.id_accion = id_accion;
                                cnx4.evalua_accion.Add(eval);
                                cnx4.SaveChanges();
                            }
                            num_indic = num_indic + 1;
                        }

                        evalua_total = evalua_acum / num_indic;
                        txtevalua.Value = evalua_total;


                    }

                }
                RadGrid1.DataSourceID = null;
                RadGrid1.DataSourceID = "EDS_EVALUA";
                Panel1.Visible = true;
            
        }
    }
}