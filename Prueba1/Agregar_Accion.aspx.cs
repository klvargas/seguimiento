﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Prueba1
{
    public partial class Agregar_Accion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void cmb_categorias_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            RadGrid1.DataSourceID = "EDS_acciones";
        }

        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            seguimientoEntities cnx1 = new seguimientoEntities();
            acciones ac = new acciones();
           
            using (cnx1)
            {
              

                ac.nombre_accion = txtnombre.Text;
                    ac.id_cat = Convert.ToInt32(cmb_categorias.SelectedValue);
                ac.descripcion = txtdescripcion.Text;


                    cnx1.acciones.Add(ac);
                    cnx1.SaveChanges();
                    string cadena_url = Request.QueryString.ToString();
                    string message = "Acción registrada correctamente";
                    string url = "Agregar_Accion.aspx?" + cadena_url;
                    string script = "{ alert('";
                    script += message; script += "');"; script += "window.location = '"; script += url; script += "'; }";
                    RadScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
            }

        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

    }
   
}