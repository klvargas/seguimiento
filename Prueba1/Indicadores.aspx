﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Indicadores.aspx.cs" Inherits="Prueba1.Indicadores" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<script runat="server">

    protected void cmb_acciones_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Indicador</h3>
              </div>
             
            </
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
<%--                    <div class="clearfix"></div>--%>
                  </div>
                  <div class="x_content">    
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Información del Indicador</span>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoría</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Categoría" ID="cmb_categoria" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_categorias" DataTextField="nombre_cat" DataValueField="id_cat" Sort="Ascending">
                      </telerik:RadComboBox>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="categorias">
                      </asp:EntityDataSource>
                    </div>
                   </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Acciones</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <telerik:RadComboBox AutoPostBack="True" EmptyMessage="Seleccione una Accion" RenderMode="Lightweight" Filter="Contains" Width="66%" ID="cmb_acciones" Runat="server" Culture="es-ES" DataSourceID="EDS_accion" DataTextField="nombre_accion" DataValueField="id_accion">
                           </telerik:RadComboBox>
                           <asp:EntityDataSource ID="EDS_accion" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="acciones" Where="it.[id_cat]=@id_cat">
                               <WhereParameters>
                                   <asp:ControlParameter ControlID="cmb_categoria" Name="id_cat" PropertyName="SelectedValue" Type="Int32" />
                               </WhereParameters>
                           </asp:EntityDataSource>
                    </div>
                   </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre </label> <br />
                       
                        <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtnombre" runat="server" style="left: 0px; top: 0px" Width="66%"></asp:TextBox>
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Unidad de Medida </label><br />
                        
                        <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txt_unidad" runat="server" style="left: 0px; top: 0px" Width="66%"></asp:TextBox>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" style="left: 0px; top: -9px">Periodicidad</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <telerik:RadComboBox EmptyMessage="Seleccione un Periodo" class="form-control" RenderMode="Lightweight" Filter="Contains" Width="66%" ID="cmb_periodicidad" runat="server" Culture="es-ES" DataSourceID="EDS_periodicidad" DataTextField="nombre" DataValueField="id_period"></telerik:RadComboBox> 
                   
                           <asp:EntityDataSource ID="EDS_periodicidad" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="periodicidad">
                           </asp:EntityDataSource>
                    </div>
                   </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" style="left: -7px; top: -11px">Responsable</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <telerik:RadComboBox EmptyMessage="Seleccione un Responsable" class="form-control" RenderMode="Lightweight" Filter="Contains" Width="66%" ID="cmb_responsable" runat="server" Culture="es-ES" DataSourceID="EDS_responsable" DataTextField="nombre" DataValueField="id_funcionario"></telerik:RadComboBox> 
                           <asp:EntityDataSource ID="EDS_responsable" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="funcionario">
                           </asp:EntityDataSource>
                    </div>
                   </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Valor Mínimo</label><br />
                          <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                              <telerik:RadNumericTextBox ID="txtmin" runat="server"></telerik:RadNumericTextBox>
                         ´</div>
                       </div>
                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Valor Máximo</label><br />
                          <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                              <telerik:RadNumericTextBox ID="txtmax" runat="server"></telerik:RadNumericTextBox>
                         ´</div>
                       </div>
                           </telerik:RadAjaxPanel>
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btn_guardar_Click"/>
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</asp:Content>
