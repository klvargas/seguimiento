﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Prueba1
{
    public partial class Indicadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            
            seguimientoEntities cnx1 = new seguimientoEntities();
            indicadores ind = new indicadores();

            using (cnx1)
            {
              
                    ind.nombre_ind = txtnombre.Text;
                    ind.unidad_medida = txt_unidad.Text;
                    ind.id_categoria = Convert.ToInt32(cmb_categoria.SelectedValue);
                    ind.id_acc = Convert.ToInt32(cmb_acciones.SelectedValue);
                    ind.id_period = Convert.ToInt32(cmb_periodicidad.SelectedValue);
                    ind.id_resp = Convert.ToInt32(cmb_responsable.SelectedValue);
                    ind.valor_min = Convert.ToInt32(txtmin.Value);
                    ind.valor_max = Convert.ToInt32(txtmax.Value);

                    cnx1.indicadores.Add(ind);
                    cnx1.SaveChanges();
                    string cadena_url = Request.QueryString.ToString();
                    string message = "Indicador registrado correctamente";
                    string url = "Indicadores.aspx?" + cadena_url;
                    string script = "{ alert('";
                    script += message; script += "');"; script += "window.location = '"; script += url; script += "'; }";
                    RadScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
                
            }
        }

       
    }
}