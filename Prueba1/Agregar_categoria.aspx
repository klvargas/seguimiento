﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Agregar_categoria.aspx.cs" Inherits="Prueba1.Agregar_categoria" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Categoría</h3>
              </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">    
                      <span class="section">Información de la Categoría</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre de Categoría </label>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtnombre" runat="server"></asp:TextBox>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Descripción </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <asp:TextBox TextMode="multiline" Columns="50" Rows ="5" CssClass="form-control col-md-7 col-xs-12" ID="txtdescripcion" runat="server"></asp:TextBox>
                            <%--<textarea id="txtdescripcion" cols="20" rows="2"></textarea>--%>
                        </div>
                      </div>
                      <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categorías</label><br />
                            <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                      <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" Culture="es-ES" DataSourceID="EDS_categorias"  Width="667px">
                        <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>
                          <MasterTableView CommandItemDisplay="Top" DataKeyNames="id_cat" DataSourceID="EDS_categorias" EditMode="Batch">
                              <CommandItemSettings AddNewRecordText="Agregar nuevo" CancelChangesText="Cancelar" RefreshText="Actualizar" SaveChangesText="Guardar" ShowAddNewRecordButton="False" />
                              <RowIndicatorColumn Visible="False">
                              </RowIndicatorColumn>
                               <ExpandCollapseColumn Created="True">
                                </ExpandCollapseColumn>
                              <Columns>
                                  <telerik:GridBoundColumn DataField="id_cat" DataType="System.Int32" FilterControlAltText="Filter id_cat column" HeaderText="id_cat" ReadOnly="True" SortExpression="id_cat" UniqueName="id_cat" Display="False">
                                  </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn ColumnEditorID= "Edit_nombre" DataField="nombre_cat" FilterControlAltText="Filter nombre_cat column" HeaderText="Nombre Categoría" SortExpression="nombre_cat" UniqueName="nombre_cat">
                                  </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="descripcion" FilterControlAltText="Filter descripcion column" HeaderText="Descripción" SortExpression="descripcion" UniqueName="descripcion">
                                  </telerik:GridBoundColumn>
                              </Columns>
                          </MasterTableView>
                      </telerik:RadGrid>
                                </div>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableFlattening="False" EntitySetName="categorias" EnableDelete="True" EnableInsert="True" EnableUpdate="True">
                      </asp:EntityDataSource>
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btn_guardar_Click" />
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                            
                        </div>
                            
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</asp:Content>
