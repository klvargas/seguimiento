﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Evalua_Accion.aspx.cs" Inherits="Prueba1.Evalua_Accion" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Valor Acción</h3>
              </div>
             <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">    
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Acción</span>
                            <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Año</label><br />
                        <div class="col-md-6 col-sm-6 col-xs-12" style="left: -1px; top: -17px ">
                            <telerik:RadComboBox ID="cmb_ano" Runat="server" Culture="es-ES" >
                            </telerik:RadComboBox>
                         </div>
                      </div>
                           <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoría</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Categoría" ID="cmb_categoria" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_categorias" DataTextField="nombre_cat" DataValueField="id_cat" Sort="Ascending">
                      </telerik:RadComboBox>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="categorias">
                      </asp:EntityDataSource>
                    </div>
                   </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Acción</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">

                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Acción" ID="cmb_accion" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_acciones" DataTextField="nombre_accion" DataValueField="id_accion" Sort="Ascending">
                      </telerik:RadComboBox>
                           <asp:EntityDataSource ID="EDS_acciones" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="acciones" Where="it.[id_cat]=@id_cat">
                           <WhereParameters>
                                   <asp:ControlParameter ControlID="cmb_categoria" Name="id_cat" PropertyName="SelectedValue" Type="Int32" />
                               </WhereParameters>
                           </asp:EntityDataSource>
                    </div>
                   </div>  
                           <asp:Panel class="x_content" ID="Panel1" runat="server" Visible="false">
                               
                           <div class="form-group">
                               <label class="control-label col-md-3 col-sm-3 col-xs-12">Evaluación</label><br />
                            <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                               <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" Culture="es-ES" DataSourceID="EDS_EVALUA">
                                   <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                                   <MasterTableView DataKeyNames="id" DataSourceID="EDS_EVALUA">
                                       <Columns>
                                           <telerik:GridBoundColumn DataField="id_indicador" DataType="System.Int32" FilterControlAltText="Filter id_indicador column" HeaderText="id_indicador" SortExpression="id_indicador" UniqueName="id_indicador" Visible="False">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="nom_indicador" FilterControlAltText="Filter nom_indicador column" HeaderText="Nombre Indicador" SortExpression="nom_indicador" UniqueName="nom_indicador">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="evaluacion" DataType="System.Double" FilterControlAltText="Filter evaluacion column" HeaderText="Evaluacion" SortExpression="evaluacion" UniqueName="evaluacion">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="promedio" DataType="System.Double" FilterControlAltText="Filter promedio column" HeaderText="Promedio" SortExpression="promedio" UniqueName="promedio">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="ano" DataType="System.Int32" FilterControlAltText="Filter ano column" HeaderText="Año" SortExpression="ano" UniqueName="ano">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="id_accion" DataType="System.Int32" FilterControlAltText="Filter id_accion column" HeaderText="id_accion" SortExpression="id_accion" UniqueName="id_accion" Visible="False">
                                           </telerik:GridBoundColumn>
                                           <telerik:GridBoundColumn DataField="id" DataType="System.Int32" FilterControlAltText="Filter id column" HeaderText="id" ReadOnly="True" SortExpression="id" UniqueName="id" Visible="False">
                                           </telerik:GridBoundColumn>
                                       </Columns>
                                   </MasterTableView>
                               </telerik:RadGrid>
                                    </div>
                               <asp:EntityDataSource ID="EDS_EVALUA" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="evalua_accion" Where="it.[id_accion]=@id_accion and it.[ano]=@ano_">
                                   <WhereParameters>
                                       <asp:ControlParameter ControlID="cmb_accion" Name="id_accion" PropertyName="SelectedValue" Type="Int32" />
                                       <asp:ControlParameter ControlID="cmb_ano" DefaultValue="ano_" Name="ano_" PropertyName="SelectedValue" Type="Int32" />
                                   </WhereParameters>
                               </asp:EntityDataSource>
                               </div>
                               <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Evaluación de la Acción<br />
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <telerik:RadNumericTextBox CssClass="form-control col-md-7 col-xs-12" style="left: 0px; top: 0px" ReadOnly="True" ID="txtevalua" runat="server" Height="30px" Width="211px"></telerik:RadNumericTextBox>
                        </div>
                      </div>
                               </asp:Panel>
                            </telerik:RadAjaxPanel>
                                  </div>
                          
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_evaluar" CssClass="btn btn-success" runat="server" Text="Evaluar" OnClick="btn_evaluar_Click" />
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                            
                        </div>
                      </div>
               </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          
</asp:Content>
