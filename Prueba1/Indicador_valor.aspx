﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Indicador_valor.aspx.cs" Inherits="Prueba1.Indicador_valor" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Valor Indicador</h3>
              </div>
             <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">    
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Información del Indicador</span>
                            <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Año</label><br />
                        <div class="col-md-6 col-sm-6 col-xs-12" style="left: -1px; top: -17px ">
                            <telerik:RadComboBox ID="cmb_ano" Runat="server" Culture="es-ES" OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged">
                            </telerik:RadComboBox>
                         </div>
                      </div>

                          <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoría</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Categoría" ID="cmb_categoria" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_categorias" DataTextField="nombre_cat" DataValueField="id_cat" Sort="Ascending">
                      </telerik:RadComboBox>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="categorias">
                      </asp:EntityDataSource>
                    </div>
                   </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Acciones</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <telerik:RadComboBox AutoPostBack="True" EmptyMessage="Seleccione una Accion" RenderMode="Lightweight" Filter="Contains" Width="66%" ID="cmb_acciones" Runat="server" Culture="es-ES" DataSourceID="EDS_accion" DataTextField="nombre_accion" DataValueField="id_accion">
                           </telerik:RadComboBox>
                           <asp:EntityDataSource ID="EDS_accion" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="acciones" Where="it.[id_cat]=@id_cat">
                               <WhereParameters>
                                   <asp:ControlParameter ControlID="cmb_categoria" Name="id_cat" PropertyName="SelectedValue" Type="Int32" />
                               </WhereParameters>
                           </asp:EntityDataSource>
                    </div>
                   </div>

                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Indicador</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">

                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione un Indicador" ID="cmb_indicador" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_indicador" DataTextField="nombre_ind" DataValueField="id_ind" Sort="Ascending" OnSelectedIndexChanged="cmb_indicador_SelectedIndexChanged">
                      </telerik:RadComboBox>
                           <asp:EntityDataSource ID="EDS_indicador" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="indicadores" EntityTypeFilter="" Select="" Where="it.[id_acc]=@id_acc">
                               <WhereParameters>
                                   <asp:ControlParameter ControlID="cmb_acciones" Name="id_acc" PropertyName="SelectedValue" Type="Int32" />
                               </WhereParameters>
                           </asp:EntityDataSource>
                    </div>
                   </div>  
                       
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Periodos</label><br />
                            <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" Culture="es-ES" DataSourceID="EDS_PERIODOS" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" Width="667px" OnNeedDataSource="RadGrid1_NeedDataSource">
                               <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                               <MasterTableView DataKeyNames="id" DataSourceID="EDS_PERIODOS" CommandItemDisplay="Top" EditMode="Batch">
                                   <CommandItemSettings AddNewRecordText="Agregar Nuevo" CancelChangesText="Cancelar" SaveChangesText="Guardar" ShowAddNewRecordButton="False" ShowSaveChangesButton="True" />
                                   <RowIndicatorColumn Visible="False">
                                   </RowIndicatorColumn>
                                   <ExpandCollapseColumn Created="True">
                                   </ExpandCollapseColumn>
                                   <Columns>
                                       <telerik:GridBoundColumn DataField="id" DataType="System.Int32" FilterControlAltText="Filter id column" HeaderText="id" ReadOnly="True" SortExpression="id" UniqueName="id" Visible="False" ForceExtractValue="Always">
                                       </telerik:GridBoundColumn>
                                       <telerik:GridBoundColumn DataField="id_indic" DataType="System.Int32" FilterControlAltText="Filter id_indic column" HeaderText="id_indic" SortExpression="id_indic" UniqueName="id_indic" Visible="False" ReadOnly="True">
                                       </telerik:GridBoundColumn>
                                       <telerik:GridBoundColumn DataField="id_periodicidad" DataType="System.Int32" FilterControlAltText="Filter id_periodicidad column" HeaderText="id_periodicidad" SortExpression="id_periodicidad" UniqueName="id_periodicidad" Visible="False" ReadOnly="True">
                                       </telerik:GridBoundColumn>
                                       <telerik:GridDropDownColumn DataField="id_periodo" DataSourceID="EDS_PER" FilterControlAltText="Filter combo_columna column" HeaderText="Periodos" ListTextField="nombre" ListValueField="id" UniqueName="combo_columna" AutoPostBackOnFilter="True" ReadOnly="True">
                                       </telerik:GridDropDownColumn>
                                       <telerik:GridNumericColumn ColumnEditorID="Edit_Valor" DataField="id_valor" DecimalDigits="4" FilterControlAltText="Filter id_valor column" SortExpression="id_valor" UniqueName="id_valor">
                                       </telerik:GridNumericColumn>
                                   </Columns>
                               </MasterTableView>
                           </telerik:RadGrid>
                                </div>
                                <asp:EntityDataSource ID="EDS_PERIODOS" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="indicador_periodos" Where="it.[id_indic]=@ID_IND_ and it.[ano]=@ANO_">
                                    <WhereParameters>
                                        <asp:ControlParameter ControlID="cmb_indicador" Name="ID_IND_" PropertyName="SelectedValue" Type="Int32" />
                                        <asp:ControlParameter ControlID="cmb_ano" Name="ANO_" PropertyName="SelectedValue" Type="Int32" />
                                    </WhereParameters>
                            </asp:EntityDataSource>
                                <asp:EntityDataSource ID="EDS_PER" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="periodos">
                            </asp:EntityDataSource>
                            <telerik:GridNumericColumnEditor ID="Edit_Valor" runat="server">
                            </telerik:GridNumericColumnEditor>
                            <br />
                            <asp:Panel class="x_content" ID="Panel1" runat="server" Visible="False">
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Evaluación<br />
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtevalua" runat="server" style="left: 0px; top: 0px" ReadOnly="True"></asp:TextBox>
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Promedio <br />
                        </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                      <telerik:RadNumericTextBox CssClass="form-control col-md-7 col-xs-12" ID="txt_prom" Runat="server" style="left: 0px; top: 0px" ReadOnly="True" Height="30px" Width="211px">
                                </telerik:RadNumericTextBox>
                        </div>
                      </div>
                            </asp:Panel>
                                </div>
                            </telerik:RadAjaxPanel>
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_evaluar" CssClass="btn btn-success" runat="server" Text="Evaluar" OnClick="btn_evaluar_Click" />
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                            <%-- <telerik:RadButton ID="btn_evaluar" runat="server" OnClick="btn_evaluar_Click" Text="Evaluar">
                            </telerik:RadButton>--%>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

</asp:Content>
