//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Prueba1
{
    using System;
    using System.Collections.Generic;
    
    public partial class funcionario
    {
        public funcionario()
        {
            this.indicadores = new HashSet<indicadores>();
        }
    
        public int id_funcionario { get; set; }
        public string nombre { get; set; }
        public Nullable<int> id_cargo { get; set; }
    
        public virtual cargo cargo { get; set; }
        public virtual ICollection<indicadores> indicadores { get; set; }
    }
}
