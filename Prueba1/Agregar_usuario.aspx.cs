﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Prueba1
{
    public partial class Agregar_usuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            seguimientoEntities cnx1 = new seguimientoEntities();
            usuarios us = new usuarios();

            using (cnx1)
            {
                us.nombre = txtnombre.Text;
                us.direccion = txtdireccion.Text;
                us.cargo = Convert.ToInt32(cmb_cargo.SelectedValue);
                
                cnx1.usuarios.Add(us);
                cnx1.SaveChanges();
                string cadena_url = Request.QueryString.ToString();
                string message = "Usuario registrado correctamente";
                string url = "Agregar_usuario.aspx?" + cadena_url;
                string script = "{ alert('";
                script += message; script += "');"; script += "window.location = '"; script += url; script += "'; }";
                RadScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);

            }
        }
    }
}