﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Prueba1
{
    public partial class Agregar_categoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            seguimientoEntities cnx1 = new seguimientoEntities();
            categorias cat = new categorias();

            using (cnx1)
            {
                
                    cat.nombre_cat = txtnombre.Text;
                    cat.descripcion = txtdescripcion.Text;


                    cnx1.categorias.Add(cat);
                    cnx1.SaveChanges();
                    string cadena_url = Request.QueryString.ToString();
                    string message = "Categoría registrado correctamente";
                    string url = "Agregar_categoria.aspx?" + cadena_url;
                    string script = "{ alert('";
                    script += message; script += "');"; script += "window.location = '"; script += url; script += "'; }";
                    RadScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
            }

        }
    
        protected void txtcodigo_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}