﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Evalua_Categoria.aspx.cs" Inherits="Prueba1.Evalua_Categoria" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Valor a la Categoría</h3>
              </div>
             <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">    
                       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Categoría</span>
                            <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Año</label><br />
                        <div class="col-md-6 col-sm-6 col-xs-12" style="left: -1px; top: -17px ">
                            <telerik:RadComboBox ID="cmb_ano" Runat="server" Culture="es-ES" >
                            </telerik:RadComboBox>
                         </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoría</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           
                      <telerik:RadComboBox AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Categoría" ID="cmb_categoria" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_categorias" DataTextField="nombre_cat" DataValueField="id_cat" Sort="Ascending">
                      </telerik:RadComboBox>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="categorias">
                      </asp:EntityDataSource>
                    </div>
                   </div>
                       
                      <asp:Panel class="x_content" ID="Panel1" runat="server" Visible="false">
                       <div class="item form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Evaluación de las Acciones</label><br />
                           <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                            <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" Culture="es-ES" DataSourceID="EDS_evalua">
                                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                                <MasterTableView DataKeyNames="id" DataSourceID="EDS_evalua">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="id" DataType="System.Int32" Display="False" FilterControlAltText="Filter id column" HeaderText="id" ReadOnly="True" SortExpression="id" UniqueName="id">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="id_cat" DataType="System.Int32" Display="False" FilterControlAltText="Filter id_cat column" HeaderText="id_cat" SortExpression="id_cat" UniqueName="id_cat">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="nom_acc" FilterControlAltText="Filter nom_acc column" HeaderText="Nombre Acción" SortExpression="nom_acc" UniqueName="nom_acc">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="evaluacion" DataType="System.Double" FilterControlAltText="Filter evaluacion column" HeaderText="Evaluación" SortExpression="evaluacion" UniqueName="evaluacion">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ano" DataType="System.Int32" FilterControlAltText="Filter ano column" HeaderText="Año" SortExpression="ano" UniqueName="ano">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                               </div>
                               <asp:EntityDataSource ID="EDS_evalua" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableFlattening="False" EntitySetName="evalua_categoria" Where="it.[id_cat]=@id_cat and it.[ano]=@ano_">
                                   <WhereParameters>
                                       <asp:ControlParameter ControlID="cmb_categoria" Name="id_cat" PropertyName="SelectedValue" Type="Int32" />
                                       <asp:ControlParameter ControlID="cmb_ano" Name="ano_" PropertyName="SelectedValue" Type="Int32" />
                                   </WhereParameters>
                            </asp:EntityDataSource>
                               </div>
                           <div class="item form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Evaluación de la Categoría <br />
                               </label>
                               <div class="col-md-6 col-sm-6 col-xs-12">
                          <telerik:RadNumericTextBox ID="txtevalua" runat="server" CssClass="form-control col-md-7 col-xs-12"  ReadOnly="True" Width="211px" Height="30px"></telerik:RadNumericTextBox>
                                   </div>
                               </div>
                           </asp:Panel>
                            </telerik:RadAjaxPanel>
                  </div>
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_evaluar" CssClass="btn btn-success" runat="server" Text="Evaluar" OnClick="btn_evaluar_Click"/>
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                            
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
</asp:Content>
