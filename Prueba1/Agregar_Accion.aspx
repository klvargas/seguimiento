﻿<%@ Page Title="" Language="C#" MasterPageFile="~/home.Master" AutoEventWireup="true" CodeBehind="Agregar_Accion.aspx.cs" Inherits="Prueba1.Agregar_Accion" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Agregar Acción</h3>
              </div>
             <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> 
            </
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>   
                   
                        <div class="x_content">    
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" height="200px" width="963px">
                      <span class="section">Información de la Acción</span>
                       
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoría</label><br />
                       <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                           <asp:ScriptManager ID="aa" runat="server"></asp:ScriptManager>
                      <telerik:RadComboBox RenderMode="Lightweight" Filter="Contains" Width="66%" EmptyMessage="Seleccione una Categoría" ID="cmb_categorias" class="form-control" Runat="server" Culture="es-ES" DataSourceID="EDS_categorias" DataTextField="nombre_cat" DataValueField="id_cat" Sort="Ascending" OnSelectedIndexChanged="cmb_categorias_SelectedIndexChanged" AutoPostBack="True">
                      </telerik:RadComboBox>
                      <asp:EntityDataSource ID="EDS_categorias" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="categorias">
                      </asp:EntityDataSource>
                    </div>
                   </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre de la Acción </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       <asp:TextBox CssClass="form-control col-md-7 col-xs-12" ID="txtnombre" runat="server" style="left: 0px; top: 0px"></asp:TextBox>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Descripción  </label>
                       
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:TextBox TextMode="multiline" Columns="50" Rows ="5" runat ="server" CssClass="form-control col-md-7 col-xs-12" ID="txtdescripcion" style="left: 0px; top: 0px"></asp:TextBox>
                        </div>
                      </div>
                       <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Acciones</label><br />
                            <div class="col-md-9 col-sm-9 col-xs-12" style="left: -1px; top: -17px">
                      <telerik:RadGrid ID="RadGrid1" runat="server" OnNeedDataSource="RadGrid1_NeedDataSource" AutoGenerateColumns="False" Culture="es-ES" DataSourceID="EDS_acciones" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True">
                <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>
                          <MasterTableView DataKeyNames="id_accion" DataSourceID="EDS_acciones" CommandItemDisplay="Top" EditMode="Batch">
                              <CommandItemSettings AddNewRecordText="Agregar Nuevo" CancelChangesText="Cancelar" RefreshText="Actualizar" SaveChangesText="Guardar" ShowAddNewRecordButton="False" ShowCancelChangesButton="True" ShowSaveChangesButton="True" />
                              <RowIndicatorColumn Visible="False">
                              </RowIndicatorColumn>
                               <ExpandCollapseColumn Created="True">
                                   </ExpandCollapseColumn>
                              <Columns>
                                  <telerik:GridBoundColumn DataField="id_accion" DataType="System.Int32" FilterControlAltText="Filter id_accion column" HeaderText="id_accion" ReadOnly="True" SortExpression="id_accion" UniqueName="id_accion" Display="False" ForceExtractValue="Always">
                                  </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="nombre_accion" FilterControlAltText="Filter nombre_accion column" HeaderText="Nombre de Acción" SortExpression="nombre_accion" UniqueName="nombre_accion">
                                  </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="id_cat" DataType="System.Int32" FilterControlAltText="Filter id_cat column" HeaderText="id_cat" SortExpression="id_cat" UniqueName="id_cat" Display="False" ForceExtractValue="Always">
                                  </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="descripcion" FilterControlAltText="Filter descripcion column" HeaderText="Descripción" SortExpression="descripcion" UniqueName="descripcion">
                                  </telerik:GridBoundColumn>
                              </Columns>
                          </MasterTableView>
                      </telerik:RadGrid>
                                </div>
                      <asp:EntityDataSource ID="EDS_acciones" runat="server" ConnectionString="name=seguimientoEntities" DefaultContainerName="seguimientoEntities" EnableFlattening="False" EntitySetName="acciones" EntityTypeFilter="" Select="" Where="it.[id_cat]=@ID_CAT" EnableDelete="True" EnableInsert="True" EnableUpdate="True">
                          <WhereParameters>
                              <asp:ControlParameter ControlID="cmb_categorias" Name="ID_CAT" PropertyName="SelectedValue" Type="Int32" />
                          </WhereParameters>
                      </asp:EntityDataSource>
                            </telerik:RadAjaxPanel>
                            </div>
                      </div>
                  </div>
                            </div>
                           
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3" style="left: 0px; top: 0px; width: 348px">
                            <asp:Button ID="btn_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btn_guardar_Click"/>
                          <button type="submit" class="btn btn-primary">Cancelar</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</asp:Content>
