﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Prueba1.Login" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
              <h1>Inicio de Sesión</h1>
              <div>
                <asp:TextBox CssClass="form-control" ID="txt_nombre" placeholder="Nombre de Usuario" runat="server" required="true"></asp:TextBox>
                <%--<input type="text" ID="txt_nombre" class="form-control" placeholder="Nombre de Usuario" required />--%>
              </div>
              <div>
                 <asp:TextBox CssClass="form-control" ID="txt_password" type="password" placeholder="Contraseña" runat="server" required="true"></asp:TextBox>
                  <%--<input type="password" ID="txt_password" class="form-control" placeholder="Contraseña" required />--%>
              </div>
              <div>
                  <asp:Button class="btn btn-default submit" ID="ingresar" runat="server" Text="Ingresar" OnClick="ingresar_Click" />
              </div>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
